Data in folder 'data'

# ----------------- #
data/cleaned_data_2.xlsx is to be used.

The address field of consignee has been cleaned up to an extent, with spelling checks.  
Punctuation and special characters have been removed.  Some of common abbrevaitions have been expanded, such ST. to STREET.  
Telephone and Fax numbers have been removed to an extent, some more regex needs to be done.
Also, for Zip code of Canada , the format has been rectified.  
The addresses are not perfectly clean however, it was a first pass.    

Also filtered the HS codes to include only those starting with 4 ( relevant to us ).

ToDo:    
Extract the destination country and Zip from the consignee address.   
Shipper addresses need to be cleaned up as well.   

------


The fields replaced with corresponding id are :

- arrival port  
- departure port  
- consignee (comapany)  
- shipper (company)  
- carrier  

The references to these id are in the files :

port_id.csv  
Carrier_id.csv  
company_names.json  

The company names need a manual pass for disambiguation, they have been clustered based on some cleaning and matching first 2 words.