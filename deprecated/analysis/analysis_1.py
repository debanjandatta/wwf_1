import pandas as pd
import networkx as nx
import pprint
import numpy as np
import operator
from collections import OrderedDict
import re
from collections import Counter
import json
import matplotlib.pyplot as plt
import plotly


def filter_by_hscode4(df):
    def aux_filter(row):
        pattern = '^4[0-9]{4,10}$'
        hs_code_parts = str(row['HS_code']).split(' ')
        for h in hs_code_parts:
            m = re.match(pattern,h)
            if m is not None and m.group(0) != '' :
                return int(h)
        return None
    df['hs_code'] = df.apply(aux_filter,axis=1)
    df = df[df['hs_code'].notnull()]
    del df['HS_code']
    return df


def set_year_month(df):
    def get_year(row):
        d = row['Arrival Date'].year
        return d

    def get_month(row):
        d = row['Arrival Date'].month
        return d


    df['year'] = df.apply(get_year,axis=1)
    df['month'] = df.apply(get_month, axis=1)

    return df

def filter_by_year(df, y):
    start_date = str(y) + '-' + '01-01'
    end_date = str(y) + '-' + '12-31'
    tmp_df = df [ (df['Arrival Date']>=start_date) & (df['Arrival Date']<=end_date)]
    print tmp_df
    return tmp_df


def get_df():
    data_loc = '../../data'
    data_file = 'cleaned_data_2.xlsx'
    data_path = data_loc + '/' + data_file
    df = pd.read_excel(data_path)
    # del df['Shipper']
    # del df['Consignee']
    # df = filter_by_hscode4(df)
    # df = set_year_month(df)
    # df.to_excel(data_path)
    return df



def sort_dict(d):
    sorted_x = sorted(d.items(), key=operator.itemgetter(1), reverse=True)
    d1 = OrderedDict()
    for item in sorted_x:
        d1[item[0]] = item[1]
    return d1


def create_graph_1():
    df = get_df()

    G = nx.Graph()
    companies_c = list(df['consignee_id'])
    companies_s= list(df['shipper_id'])

    for c,s in zip(companies_c,companies_s):

        if not G.has_node(c):
            G.add_node(c)
        if not G.has_node(s):
            G.add_node(s)
        if not G.has_edge(s,c):
            G.add_edge(s,c,count = 1)
        else:
            e = G.get_edge_data(s,c)
            count = e['count']
            count += 1
            attr = { (s,c) : {'count': count} }
            nx.set_edge_attributes(G,attr )

    pprint.pprint('Number of nodes ' + str(G.number_of_nodes()) )
    pprint.pprint('Number of edges ' + str(G.number_of_edges()) )

    print '-------------'
    # Calculate betweenness centrality
    bw_centrality = nx.betweenness_centrality(G)
    bw_centrality = sort_dict(bw_centrality)
    nx.set_node_attributes(G, bw_centrality, 'bw_centrality')
    print '-------------'

    # Calculate degree centrality
    deg_centrality = nx.degree_centrality(G)
    deg_centrality = sort_dict(deg_centrality)
    nx.set_node_attributes(G, deg_centrality, 'deg_centrality')

    print '-------------'

    # Calculate degree centrality
    deg_centrality = nx.degree_centrality(G)
    deg_centrality = sort_dict(deg_centrality)
    nx.set_node_attributes(G, deg_centrality, 'deg_centrality')

    print '--------------'

    pagerank = nx.pagerank(G, alpha=0.85)
    pagerank = sort_dict(pagerank)
    nx.set_node_attributes(G, pagerank, 'pagerank')

    return G
#
# create_graph_1()

def analyze_by_unitcost():

    def calc_unit_cost(row):
        res = float(row['estimatedValue']/row['Weight'])
        return res


    df = get_df()
    df['unit_cost'] = df.apply(calc_unit_cost, axis=1)

    # group by HS codes
    c_map = {}
    hs_codes = list(set(df['hs_code']))
    # pprint.pprint(hs_codes)
    for _hs_code in hs_codes :
        tmp_df = df[ (df['hs_code'] == _hs_code)]

        prices = list(tmp_df['unit_cost'])
        if len(prices) < 25 :
            continue

        alpha = 5.0
        _pc_1 = np.percentile(prices, alpha)
        _pc_2 = np.percentile(prices, 100 - alpha)
        tmp_df_1 = tmp_df[(tmp_df['unit_cost'] <= _pc_1)]

        if len(tmp_df_1) > 0 :
            print '----------- hs code ', _hs_code
            print tmp_df_1['shipper_id']
            print tmp_df_1['consignee_id']
            print '-----------'
            c_list = []
            c_list.extend(list(tmp_df_1['shipper_id']))
            c_list.extend(list(tmp_df_1['consignee_id']))
            c_map [_hs_code] = c_list

    # find repeaters
    vals = c_map.values()
    flat_list = [item for sublist in vals for item in sublist]
    cnt = Counter(flat_list)

    pprint.pprint(cnt)
    company_file  = '../../data/synthetic/company_names.json'
    with open(company_file) as data_file:
        data = json.loads(data_file.read())

    for k,v in cnt.iteritems():
        print data[str(k)][0] ,  v

# analyze_by_unitcost()

#
# df = get_df()
# df = filter_by_year(df,2007)

def analyze_2():
    df = get_df()

    def get_yr_quarter(row):

        if row['month'] <= 3 :
            res = 1
        elif row['month'] > 3 and row['month'] <= 6 :
            res = 2
        elif row['month'] >6 and row['month'] <=9 :
            res = 3
        else :
            res = 4
        return row['year'] + float(res)/10

    def calc_unit_cost(row):
        res = float(row['estimatedValue'] / row['Weight'])
        return res

    df['quarter'] =  df.apply(get_yr_quarter,axis =1)
    df['unit_cost'] = df.apply(calc_unit_cost, axis=1)
    df = df.sort_values(by=['quarter'])
    hs_codes = list(set(df['hs_code']))
    # pprint.pprint(hs_codes)

    for _hs_code in hs_codes:
        tmp_df = df[(df['hs_code'] == _hs_code)]
        uc = list(tmp_df['unit_cost'])
        q = list(tmp_df['quarter'])
        plt.plot(q,uc,'rx')
        plt.title( 'HS Code '+ str(_hs_code))
        # plt.show()
        tmp_df1 = pd.DataFrame(tmp_df.groupby(by=['hs_code','quarter'])['Weight','estimatedValue'].agg('sum')).reset_index()


        q = tmp_df1['quarter']
        w = tmp_df1['Weight']
        v = tmp_df1['estimatedValue']

        if len(q) <5 :
            continue

        color = 'tab:red'
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('Year, Qaurter')
        ax1.set_ylabel('Weight', color=color)
        ax1.plot(q, w, 'r.-')
        ax1.tick_params(axis='y', labelcolor=color)

        ax2 = ax1.twinx()
        color = 'tab:blue'
        ax2.set_ylabel('Estimated Value', color=color)  # we already handled the x-label with ax1
        ax2.plot(q, v, 'b.-')
        ax2.tick_params(axis='y', labelcolor=color)
        plt.title('HS Code ' + str(_hs_code))
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.show()


def analyze_3():
    G = create_graph_1()
    print nx.is_connected(G)
    conn_comp =0
    for i in nx.connected_components(G):
        conn_comp += 1
        print 'Size of connected component ', len(i)
    print 'Number of components', conn_comp


def analyze_4():

    df = get_df()
    all_hs_codes = set(df['hs_code'])

    s = list(df['shipper_id'])
    c = list(df['consignee_id'])
    all_companies = list(set(s+c))
    G = nx.Graph()

    # create the graph nodes
    G.add_nodes_from(all_companies)
    print 'Number of nodes', G.number_of_nodes()

    # Local function
    def group_by_hs_code(tmp_df, hs_code):
        _tmp_df = pd.DataFrame(tmp_df,copy=True)
        print '------------'
        # print '>> ',hs_code

        def _aux_filter(row, _hs):
            _hs = str(_hs)
            h = str(row['hs_code'])
            m = re.match(_hs, h)
            if m is not None and m.group(0) != '' :
                return _hs
            else :
                return None

        _tmp_df['hs_code'] = _tmp_df.apply(_aux_filter,axis = 1,_hs = hs_code )
        _tmp_df = _tmp_df[_tmp_df.hs_code.notnull()]

        if len(_tmp_df) == 0 :
            print tmp_df[tmp_df['hs_code']==int(_hs)]

        return _tmp_df



    for _hs in all_hs_codes :
        tmp_df = pd.DataFrame(df,copy=True)
        tmp_df = group_by_hs_code(tmp_df,_hs)
        print  _hs, ' | ', len(tmp_df)

        companies_c = tmp_df['consignee_id']
        companies_s = tmp_df['shipper_id']

        for c, s in zip(companies_c, companies_s):

            if not G.has_node(c):
                G.add_node(c)
            if not G.has_node(s):
                G.add_node(s)
            if not G.has_edge(s, c):
                G.add_edge(s, c, weight=1)
            else:
                e = G.get_edge_data(s, c)
                count = e['weight']
                count += 1
                attr = {(s, c): {'weight': count}}
                nx.set_edge_attributes(G, attr)

    node_label_dict = {}

    for i in all_companies :
        node_label_dict[i] =  str(i)
    plt.figure(figsize=[20,15])
    # nx.draw_networkx_nodes(G, pos=nx.spring_layout(G),color='r',node_size=75 )
    # nx.draw_networkx_edges(G,pos=nx.spring_layout(G), width=0.15,edge_color='b',alpha=0.75)
    nx.draw_spring(G,
                   node_size=550,
                   node_color='r',
                   edge_color='b',
                   alpha=0.85,
                   labels = node_label_dict,
                   font_size = 5
    )
    plt.show()



analyze_4()
