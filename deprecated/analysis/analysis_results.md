Analysis of transactions based on price per unit.
Using 'estimatedValue' in dollars, and 'Weight' - calculate the cost per unit.
The transactions which are below 5th percentile of cost per unit are taken for each hs code,given adequate number of transactions exist.
The idea here is if they are mis-representing an HS Code it can turn up as similar products have similar prices.
The companies which turn up are :

- fung fei international ltd 3
- l h unique pte ltd 2
- hunchun xingjia wooden flooring 1
- pantim wood prodcts inc 16
- dalian brilliant wood products 1
- dalian jiayang wood products co 1
- prime supply central pl s 5
- dalian kemian wood industry 3
- provenza floors inc 1
- pt jati luhup agung 1
- daliankemian wood industry co ltd 1
- beijing lanhaitong co ltd guomen 1
- benxi flooring factory 4
- qingdao tongruilong trade 2
- brandywine international hardwod 2
- suzhou long chain household co 1
- taizhou raytium outdoor 1
- rona 8
- the king of floor ltd 1
- to the order of pantim 3
- sck wooden industries sdn bhd 11
- hongkong jian hua int l trade co lt 3


--------------------

Created a graph with nodes as company ids.
An edge is added if (u,v) have a trade record.
Edge weight  = Number of trade records containing u and v

Number of components 8
Size of connected component  573
Size of connected component  3
Size of connected component  2
Size of connected component  2
Size of connected component  3
Size of connected component  2
Size of connected component  2
Size of connected component  2



----------------------
----------------------



 HS Code | Number of trade records 
 
440320  |  6    
441600  |  51   
440710  |  39   
440200  |  1    
440920  |  10165    
440723  |  6    
44182   |  220  
442010  |  9    
44079   |  481  
44189   |  26   
441890  |  25   
441510  |  5    
44071   |  40   
441900  |  1    
401693  |  2    
441520  |  9    
441010  |  6    
440500  |  2    
441400  |  9    
440121  |  1    
440890  |  8    
44219   |  706  
44092   |  10172    
491199  |  2    
440130  |  1    
442190  |  705  
440399  |  35   
441299  |  1    
440791  |  256  
440792  |  18   
440420  |  25   
441820  |  219  
440799  |  206  
441700  |  42   
441830  |  4    
440721  |  2     
440310  |  1    
442110  |  1    


----------------------
----------------------








