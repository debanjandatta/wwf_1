import pandas as pd
import numpy as np
import re
import os
import sys
import pprint
import re
import nltk
import os.path
import importlib
# import company_name_spell_check
from src.preprocess import cleanup_helper


def import_data():

    cwd = os.getcwd()
    print(os.getcwd())
    os.chdir('./../..')
    print(os.getcwd())
    data_loc = 'data/'
    data_file = data_loc + 'US_IMPORTS_BillsofLading_PIERS_ForestProducts_WWF_2007-2013_21Companies.xlsx'

    data_file = 'new_1.xlsx'
    print (data_file)

    try :
        df = pd.read_excel(io=data_file, sheet_name=0)
    except :
        print ('Error reading file')
        os.chdir(cwd)
        return None
    os.chdir(cwd)

    attrs = [
        'Source Country',
        'Arrival Date',
        'estimatedValue',
        'portArrivalOrig',
        'portDepartureOrig',
        'Consignee',
        'Consignee Address',
        'Shipment Address',
        'Carrier',
        'Harmonized Tariff Schedule',
        'coastalRegion',
        'Shipper',
        'regionOfOrigin',
        'Weight',
        'commodityShortDesc',
        'COMMODITY_DESC'
    ]

    df = df.loc[:, attrs]
    return df

# --------- Set up ids for the Carriers --------- #
def setup_carriers(df1):
    carriers = sorted (set(df1['Carrier']))
    pprint.pprint(sorted(carriers))

    c_id = 1
    c_map = {}
    for c in carriers :
        c_map[c_id] = str(c)
        c_id += 1

    c_df = pd.DataFrame(c_map.items(), columns=['carrier_id', 'carrier'])
    c_df.to_csv('Carrier_id.csv')

    def set_carr_id(row):
        p = row['Carrier']
        for k, v in c_map.iteritems():
            if v == p:
                return k

    df1['carrier_id'] = df1.apply(set_carr_id, axis=1)
    return df1

# --------- Set up ids for the Ports ------------ #
def setup_ports(df1):
    ports = list(set(df1['portDepartureOrig']))
    ports.extend(set(df1['portArrivalOrig']))

    all_ports = sorted([str(p) for p in ports])
    valid_ports = []
    for a in all_ports:
        m = re.search('([0-9])+', a)
        if m is None:
            valid_ports.append(a)
        else:
            print (a)

    port_id = 1
    port_dict = {}
    for p in valid_ports:
        port_dict[port_id] = p
        port_id += 1

    port_df = pd.DataFrame(port_dict.items(), columns=['port_id', 'port'])
    port_df.to_csv('port_id.csv')

    def set_port_id(row,attr):
        p = row[attr]
        for k,v in port_dict.items():
            if v == p:
                return k

    df1['dep_port_id'] = df1.apply(set_port_id,axis=1,attr ='portDepartureOrig')
    df1['ariv_port_id'] = df1.apply(set_port_id,axis=1,attr ='portArrivalOrig')
    return df1


# ----------------------------------------------- #
#  Clean up the company names and collate them
# ----------------------------------------------- #

def clean_up(df):

    consignee = list(df['Consignee'])
    shipper = list(df['Shipper'])
    # get list of all companies
    companies = list(shipper)
    companies.extend(consignee)
    companies = list(sorted(companies))

    def clean_cname_1(row ,attr):
        return cleanup_helper.clean_up_cname_str(row[attr])

    df = import_data()
    df['consignee'] = df.apply(
        clean_cname_1,
        axis=1,
        attr='Consignee'
    )
    df['shipper'] = df.apply(
        clean_cname_1,
        axis=1,
        attr='Shipper'
    )

    del df['Shipper']
    del df['Consignee']

    def clean_adr_1(row ,attr):
        return cleanup_helper.init_addr_clean(row[attr])

    df['consignee_address'] = df.apply(
        clean_adr_1,
        axis=1,
        attr='Consignee Address'
    )

    df['shippment_address'] = df.apply(
        clean_adr_1,
        axis=1,
        attr='Shipment Address'
    )

    return df

df = import_data()
df = clean_up(df)

def cluster_companies(df):

    consignee = list(df['consignee'])
    shipper = list(df['shipper'])
    # get list of all companies
    companies = list(shipper)
    companies.extend(consignee)
    companies = list(sorted(companies))
    list_1 = []
    for i in companies :
        list_1.append(nltk.tokenize(words))



# df1 = df1.rename(columns={'Harmonized Tariff Schedule':'HS_code'})
# df1 = spell_check_1.correct_address(df1)
# df1 = setup_carriers(df1)
# df1 = setup_ports(df1)
#
# del df1['Carrier']
# del df1['portArrivalOrig']
# del df1['portDepartureOrig']
# df1.to_excel('new_file.xlsx')