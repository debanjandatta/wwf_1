import pandas as pd
import re
import os
import pprint
import nltk
import json

def import_data():
    cwd = os.getcwd()
    os.chdir('./../../')
    print os.getcwd()
    data_loc = 'data/'
    data_file = data_loc + 'cleaned_data_1.xlsx'
    print data_file
    try :
        df1 = pd.read_excel(io=data_file, sheet_name=0)
    except :
        print 'Error reading file'
        os.chdir(cwd)
        return None
    os.chdir(cwd)
    return df1

df1 = import_data()

print df1.columns
consignee =  list(sorted(set(df1['Consignee'])))
shipper = list(sorted(set(df1['Shipper'])))

def clean_up_1(a):
    a = str(a)
    z = a.strip('\n\t')
    z = z.strip('\n')
    z = z.strip('#')
    z = z.strip('$')
    z = z.strip('.')
    z = z.strip()

    removal_chars = ['.','-','#',"'", '@', '$', '?', '=', ':', ',', ';', '&', ']', '[', '(', ')', '{', '}', '`', '/', '\\']
    for ch in removal_chars:
        z = str.replace(z, ch, ' ')

    a = str(z).lower()
    tokens = nltk.word_tokenize(a)
    pattern = '[0-9]+'
    res = []
    for z in tokens :

        match = re.search(pattern, z)
        if match is not None and match.group(0) != '':
            continue

        res.append(z)
    res = ' '.join(res)
    return res

companies = list(shipper)
companies.extend(consignee)
companies = list(sorted(set(companies)))
companies = [ clean_up_1(c) for c in companies]
companies = list(sorted(set(companies)))
pprint.pprint( companies )

# companies = [ nltk.word_tokenize(c) for c in companies]

cluster = []
max_len = len(companies)
# cluster based on first 2 words

idx1 = 0
while idx1 < len(companies):
    tokens =  nltk.word_tokenize(companies[idx1])
    token1 = tokens[0]
    pattern = token1
    if len(tokens) > 1 :
        token2 = tokens[1]
        pattern += '\s'+token2 + '\s|$'

    cur_cluster = [companies[idx1]]
    idx2 = 0
    for idx2 in range(idx1, max_len):
        m = re.match(pattern,companies[idx2])
        if m is None:
            break
        else :
            cur_cluster.append(companies[idx2])

    if idx2 ==  idx1 :
        idx1 += 1
    else:
        idx1 = idx2
    cluster.append(cur_cluster)

companies_map = {}
c_id = 1
for c in cluster:
    companies_map[c_id] = c
    c_id += 1

# write the data out
loc = '../../data/synthetic'
company_id_file = loc + '/' + 'company_names.json'
with open(company_id_file, 'w') as outfile:
    r = json.dumps(companies_map,indent=4)
    outfile.write(r)
    outfile.close()


def replace_company(row, attr):
    adr = row[attr]
    adr = clean_up_1(adr)

    for id,name_list in companies_map.iteritems() :
        if adr in name_list:
            return id


df1['consignee_id'] = df1.apply(replace_company,
                                axis=1,
                                attr='Consignee')
df1['shipper_id'] = df1.apply(replace_company,
                              axis=1,
                              attr='Shipper')


data_file = '../../data' + '/'+ 'cleaned_data_2.xlsx'
df1.to_excel(data_file)


print ('---------------')


