import re
from collections import Counter
from collections import OrderedDict
import pandas as pd
import nltk
nltk.download('punkt')
import sys
import operator

abbr_df = pd.read_csv('address_abbr.txt',header=None)
abbr_dict = {}
for a,b in zip(list(abbr_df[0]),list(abbr_df[1])):
    abbr_dict[str(a).lower()] = str(b).lower()



def expand_abbr(adr) :
    global abbr_dict
    tokens = nltk.word_tokenize(adr)
    res = []
    for t in tokens:
        cur = t
        for key, value in abbr_dict.iteritems():
            if t == key:
                cur = value
        res.append(cur)

    res = ' '.join(res)
    return res

def fix_address_string(a):
    global abbr_dict
    a = a.lower()
    # Check for Canadian Zip code
    m = re.search('[a-y]{1}[0-9]{1}[a-y]{1}\s{1}[0-9]{1}[a-z]{1}[0-9]{1}', a)
    if m is not None:
        pos = m.start()
        s = list(a)
        s[pos + 3] = '-'
        a = ''.join(s)

    a = clean_address(a)
    res = expand_abbr(a)
    return res

def clean_address(adr) :

    adr = str(adr)
    strip_chars = [ '\n\t', '\n','#' ,'$' , '.' , ' ' ]
    for ch in strip_chars :
        adr = adr.strip(ch)


    removal_chars = ['@','$','?','=',':',',',';','&',']','[','(',')','{','}','`','/','\\']
    for ch in removal_chars:
        z = str.replace(z, ch, ' ')

    a = str(z).lower()
    return a

def get_words(tokens):
    res = []
    pattern = '(\+)*([0-9]+(-|:|.)*)+([0-9])+'
    for p in tokens:
        m = re.match(pattern, p)
        if m is None or m.group() == '':
           res.append(p)
    return res

def create_address_corpus():
    corpus = []
    fname ='country_names.txt'
    with open(fname) as f:
        content = f.readlines()

    content = [ str(c).strip('\n') for c in content ]
    corpus.extend(content)

    fname = 'city_names.txt'
    with  open(fname) as f:
        content = f.readlines()

    content = [ str(c).strip('\n') for c in content ]
    corpus.extend(content)

    data_loc = '../../data/'
    data_file = data_loc + 'US_IMPORTS_BillsofLading_PIERS_ForestProducts_WWF_2007-2013_21Companies.xlsx'

    df1 = pd.read_excel(io=data_file, sheet_name=0)
    a_set = (list(df1['Consignee Address']))
    res = []
    for a in a_set:
        a = clean_address(a)
        tokens = nltk.word_tokenize(a)
        tokens = get_words(tokens)
        res.extend(tokens)
    res = list(res)
    corpus.extend(res)
    corpus = list(sorted(corpus))

    _corpus = []
    # post process
    corpus = list(sorted(corpus))
    for c in corpus:
        c = str(c)
        c = str(c.decode('unicode_escape').encode('ascii', 'ignore'))
        pattern = '([0-9])+'
        m = re.search(pattern,c)
        if m is not None:
            continue

        if len(c)<3:
            continue

        c = c.lower()
        if ' ' in c:
            parts = c.split(' ')
            _corpus.extend(parts)
        else:
            _corpus.append(c)

    corpus = list(sorted(_corpus))
    with open('corpus.txt','w') as f:
        for item in corpus:
           f.write(str("%s\n") % item)



# ------------------------------------------- #

def read_words(text):
    res = re.findall(r'\w+', text.lower())
    return res

all_words = Counter(read_words(open('corpus.txt').read()))

def known(words):
    global all_words
    d = {}
    for w in words:
        if w in all_words :
            d[w] =  all_words[w]
    return d


def correction(word):
    if len(word) < 3 :
        return word
    no_edit = known(word)
    edits_1 = known(edits1(word))
    edits_2 = known(edits2(word))
    edits = dict(no_edit.items() + edits_1.items() + edits_2.items() )
    edits = sorted(edits.items(), key=operator.itemgetter(1),reverse=True)
    if len(edits) > 0:
        return edits[0][0]
    else :
        return word


def edits1(word):
    letters = 'abcdefghijklmnopqrstuvwxyz'
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [L + R[1:] for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R) > 1]
    replaces = [L + c + R[1:] for L, R in splits if R for c in letters]
    inserts = [L + c + R for L, R in splits for c in letters]
    res = set(deletes + transposes + replaces + inserts)
    return res

def edits2(word):
    "All edits that are two edits away from `word`."
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))

#-----------------------#

# create_address_corpus()

data_loc = '../../data/'
def aux_corr_add(row, attr):
    a = row[attr]
    a = fix_address_string(a)
    a = nltk.word_tokenize(a)
    r = []
    if len(a) < 2:
        r = a
    else :
        for _a in a:
            r.append(correction(_a))
        r = ' '.join(r)
    return r

def correct_address(dataframe):
    attr = 'Consignee Address'
    dataframe['consignee_address'] = dataframe.apply(aux_corr_add, axis=1, attr = attr)
    return dataframe


# data_file = data_loc + 'US_IMPORTS_BillsofLading_PIERS_ForestProducts_WWF_2007-2013_21Companies.xlsx'
# df1 = pd.read_excel(io=data_file, sheet_name=0)
# df1 = correct_address(df1)
# df1.to_excel('new_file.xlsx')