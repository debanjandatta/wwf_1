import pandas as pd
import nltk
import re


def clean_up_cname_str(name):

    name = str(name)

    strip_chars = ['\n\t', '\n', '#', '$', '.', ' ']

    for ch in strip_chars:
        name = name.strip(ch)

    # Remove special characters
    removal_chars = [
        '-', '#', "'",
        '@', '$', '?', '=',
        ':', ',', ';', '&',
        ']', '[', '(', ')',
        '{', '}', '`', '/',
        '\\'
    ]


    for ch in removal_chars:
        name = str.replace(name, ch, ' ')
    # treat a . differently
    name = str.replace(name, '.', '. ')

    # Remove numeric strings
    name = str(name).lower()
    tokens = nltk.word_tokenize(name)
    pattern = '[0-9]+'
    res = []
    for z in tokens:
        if z == ' ':
            continue
        match = re.search(pattern, z)
        if match is not None and match.group(0) != '':
            continue
        res.append(z)

    res = ' '.join(res)
    return res



