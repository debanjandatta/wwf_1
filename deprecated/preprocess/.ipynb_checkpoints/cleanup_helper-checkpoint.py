import pandas as pd
import nltk
import re

abbr_dict = {}
def set_up_abbr_dict():
    abbr_df = pd.read_csv('auxillary/address_abbr.txt',header=None)
    global abbr_dict
    for a,b in zip(list(abbr_df[0]),list(abbr_df[1])):
        abbr_dict[str(a).lower()] = str(b).lower()


def clean_up_cname_str(name):
    name = str(name)
    strip_chars = ['\n\t', '\n', '#', '$', '.', ' ']
    for ch in strip_chars:
        name = name.strip(ch)

    # Remove special characters
    removal_chars = [
        '-', '#', "'",'>','<','`',
        '@', '$', '?', '=',
        ':', ',', ';', '&',
        ']', '[', '(', ')',
        '{', '}', '`', '/',
        '\\'
    ]

    for ch in removal_chars:
        name = str.replace(name, ch, ' ')
    # treat a . differently
    name = str.replace(name, '.', '. ')

    # Remove numeric strings
    name = str(name).lower()
    tokens = nltk.word_tokenize(name)
    pattern = '[0-9]+'
    res = []
    for z in tokens:
        if z == ' ':
            continue
        match = re.search(pattern, z)
        if match is not None and match.group(0) != '':
            continue
        res.append(z)
    res = ' '.join(res)
    return res


# -------------------------- #

def init_addr_clean(adr):
    adr = str(adr)
    adr = adr.lower()

    strip_chars = ['\n\t', '\n', '#', '$', '.', ' ']
    for ch in strip_chars:
        adr = adr.strip(ch)

    removal_chars = ['@', '$', '?', '=', ':', ',', ';', '&', ']', '[', '(', ')', '{', '}', '`', '/', '\\']
    for ch in removal_chars:
        adr = str.replace(adr, ch, ' ')

    # Search if there is a format matching Zip code of Canada
    m = re.search('[a-y]{1}[0-9]{1}[a-y]{1}\s{0,1}[0-9]{1}[a-z]{1}[0-9]{1}', adr)

    if m is not None:
        start = m.start()
        end = m.end()
        s = list(adr)
        if end - start < 7 :
            pos = start + 4
            s.insert(pos,'-')
        else :
            pos = start + 3
            s[pos] = '-'
        adr = ''.join(s)


    return adr