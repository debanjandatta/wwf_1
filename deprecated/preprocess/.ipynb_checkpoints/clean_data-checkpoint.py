import pandas as pd
import numpy as np
import re
import os
import sys
import pprint
import re
import spell_check_1



def import_data():
    cwd = os.getcwd()
    os.chdir('./../../')
    print os.getcwd()
    data_loc = 'data/'
    data_file = data_loc + 'US_IMPORTS_BillsofLading_PIERS_ForestProducts_WWF_2007-2013_21Companies.xlsx'
    print (data_file)
    try :
        df1 = pd.read_excel(io=data_file, sheet_name=0)
        df2 = pd.read_excel(io=data_file, sheet_name=1)
        df3 = pd.read_excel(io=data_file, sheet_name=2)
    except :
        print ('Error reading file')
        os.chdir(cwd)
        return None
    os.chdir(cwd)
    return df1,df2,df3


def setup_carriers(df1):
    carriers = sorted (set(df1['Carrier']))
    pprint.pprint(sorted(carriers))

    c_id = 1
    c_map = {}
    for c in carriers :
        c_map[c_id] = str(c)
        c_id += 1

    c_df = pd.DataFrame(c_map.items(), columns=['carrier_id', 'carrier'])
    c_df.to_csv('Carrier_id.csv')

    def set_carr_id(row):
        p = row['Carrier']
        for k, v in c_map.iteritems():
            if v == p:
                return k

    df1['carrier_id'] = df1.apply(set_carr_id, axis=1)
    return df1

def setup_ports(df1):
    ports = list(set(df1['portDepartureOrig']))
    ports.extend(set(df1['portArrivalOrig']))

    all_ports = sorted([str(p) for p in ports])
    valid_ports = []
    for a in all_ports:
        m = re.search('([0-9])+', a)
        if m is None:
            valid_ports.append(a)
        else:
            print a

    port_id = 1
    port_dict = {}
    for p in valid_ports:
        port_dict[port_id] = p
        port_id += 1

    port_df = pd.DataFrame(port_dict.items(), columns=['port_id', 'port'])
    port_df.to_csv('port_id.csv')

    def set_port_id(row,attr):
        p = row[attr]
        for k,v in port_dict.iteritems():
            if v == p:
                return k

    df1['dep_port_id'] = df1.apply(set_port_id,axis=1,attr ='portDepartureOrig')
    df1['ariv_port_id'] = df1.apply(set_port_id,axis=1,attr ='portArrivalOrig')
    return df1



df1, _, _ = import_data()
attrs = [
    'Source Country',
    'Arrival Date',
    'estimatedValue',
    'portArrivalOrig',
    'portDepartureOrig',
    'Consignee',
    'Consignee Address',
    'Shipment Address',
    'Carrier',
    'Harmonized Tariff Schedule',
    'coastalRegion',
    'Shipper',
    'regionOfOrigin',
    'Weight'
]
df1 = df1.loc[:,attrs]

# df1 = df1.rename(columns={'Harmonized Tariff Schedule':'HS_code'})
df1 = spell_check_1.correct_address(df1)
df1 = setup_carriers(df1)
df1 = setup_ports(df1)

del df1['Carrier']
del df1['portArrivalOrig']
del df1['portDepartureOrig']
df1.to_excel('new_file.xlsx')