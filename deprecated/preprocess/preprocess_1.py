import pandas as pd
import os
import sys
sys.path.append('./../..')


def import_data():
    data_loc = './../../data/'
    data_file = data_loc + 'US_IMPORTS_BillsofLading_PIERS_ForestProducts_WWF_2007-2013_21Companies.xlsx'
    try :
        df = pd.read_excel(data_file)
        return df
    except :
        print 'Error reading file'
        exit(1)
    return None


