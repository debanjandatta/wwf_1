# ---------------------------------------- #
# This script takes in the csv with the combined full description with the notations
# Then processes each description as an infix arithmetic notation ( e.g [A+[B+[C+D]]] ) using a stack
# It separates the positive and negatve elements, and for description extracts the tokens.
# Lemmatization is done, after which keywords are extracted - which may be single nouns or maybe phrases
# --------------------------------------- #

import pandas as pd
import spacy
import re
nlp = spacy.load('en')

def get_df ():
    file_path = '2017_02_02.csv'
    df = pd.read_csv(file_path)
    return df


def get_kws(sent):
    tmp_doc = nlp(sent)
    pos_tags = []
    word_list = []

    for e in tmp_doc:
        if e.tag_ == 'NN':
            symbol = '1'
        elif e.tag_ == 'JJ':
            symbol = '2'
        elif e.pos_ == 'VERB':  # Multiple verb forms often used as adjectives
            symbol = '3'
        elif e.tag_ == 'HYPH':
            symbol = '4'
        else:
            symbol = '0'

        pos_tags.append(symbol)
        word_list.append(str(e))

    kw = []
    pattern = "((1){1}(2|3){0,1}(1){0,2})|(((1|2|3){0,2}(3){0,1}(1){0,2}))|((1){1})|((1|2|3){1})"
    pos_tags = ''.join(pos_tags)
    m = re.search(pattern, pos_tags)
    i = 0
    while m is not None and m.group(i) is not None:
        cur_match = m.group(i)
        s_pos = m.start(i)
        e_pos = m.end(i)
        res = (word_list[s_pos:e_pos])
        res = ' '.join(res)
        i += 1
        kw.append(res)
    return kw


def parse_sent(sent):
    # Each punctuation is mentioned as '; &' or '& &'
    # Split by '& &' and then by '; &'
    parts_1 = sent.split('& &')
    res = []
    for part in parts_1:
        parts_2 = part.split('& ;')
        for part_2_i in parts_2:
            phrase = ''.join(part_2_i)
            phrase = part_2_i.strip()
            kw = get_kws(phrase)
            res.extend(kw)
    return res


def process_line(tokens):
    stack = []
    is_neg = False
    pos_kw = []
    neg_kw = []

    for i in range(len(tokens)):
        # Lemmatization
        t = str(tokens[i].lemma_)
        is_neg = False
        if t == "[" or t == "[[":
            stack.append(t)
        elif t == "]":
            cs = []
            while (len(stack) > 0):
                z = stack.pop()
                if z == '[':
                    break
                else:
                    cs.append(z)
            cs = list(reversed(cs))
            sent = ' '.join(cs)
            kws = parse_sent(sent)

            if len(stack) > 0 and stack[-1] == '!':
                is_neg = True
                # remove the !
                stack.pop()

            if kws is not None and len(kws) > 0:
                res = []
                for k in kws:
                    if len(k) > 0:
                        res.append(k)

                if is_neg:
                    neg_kw.extend(res)
                else:
                    pos_kw.extend(res)
        elif t == '&':
            stack.append(t)
        elif t == '!':
            stack.append(t)
        else:
            stack.append(t)

    # remove duplicates
    pos_kw = list(set(pos_kw))
    neg_kw = list(set(neg_kw))

    pos_kw = ';'.join (pos_kw)
    neg_kw = ';'.join (neg_kw)
    return pos_kw, neg_kw


# doc = nlp(z)
# tokens = [t for t in doc]
# pos_kw, neg_kw = process_line(tokens)
# print(pos_kw, neg_kw)


def get_df_kws(row):
    z = row['full_desc']
    z = z.replace(';','&;')
    nlp = spacy.load('en')
    doc = nlp(z)
    tokens = [t for t in doc]
    pos_kw, neg_kw = process_line(tokens)
    print ( '||+|| ',pos_kw,' ||-||', neg_kw )
    row['pos_kw'] = pos_kw
    row['neg_kw'] = neg_kw
    return row


df = get_df()
df = df.apply(get_df_kws, axis=1)
del df['Unnamed: 0']
print (df)
df.to_csv('2017_02_02_kw.csv')
