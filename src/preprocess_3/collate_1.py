import pandas as pd
import re

def get_df(file_loc, file_name):
    f_path = file_loc + '/' + file_name
    df = pd.read_csv(f_path)
    df = df[['HTS Number', 'Indent', 'Description']]
    df = df.reset_index()
    return df


def gen_aux_desc(df,idx):
    min_idx = 0
    neg_keyword = 'Other'
    res = None
    cur_row = df.loc[idx]
    
    cur_indent = cur_row['Indent']
    
    cur_desc = cur_row['Description']
    is_neg_keyword = False

    if type(cur_desc) != str:
        cur_desc = ''

    if cur_desc == neg_keyword:
        is_neg_keyword = True

    def format_desc(s):
        return '[ '+ s + ' ]'

    if cur_indent == 0 :
        res = format_desc(cur_desc)
    else:
        print ( idx )
        # go up and fetch
        cur_idx = idx - 1

        if is_neg_keyword:

            res_parts = []

            while cur_idx >= min_idx:
                r = df.loc[cur_idx]
                # Find entry in same level
                if r['Indent'] == cur_indent:
                    # add in what the thing is not
                    d3 = r['Description']
                    if type(d3) == str and len(d3)>0:
                        t = '!' + format_desc(d3)
                        res_parts .append(t)

                elif r['Indent'] == cur_indent-1:
                        fd = r['full_desc']
                        res = ' && '.join(res_parts)
                        res = format_desc(res)
                        res = fd + ' && ' + res
                        res = format_desc(res)
                        break

                cur_idx -= 1


        else:
            while cur_idx >= min_idx:
                r = df.loc[cur_idx]
                dsc = r['full_desc']
                if r['Indent'] == cur_indent-1 :
                    res = dsc + ' && ' + format_desc(cur_desc)
                    break
                else :
                    cur_idx -= 1

    print (res)
    return res

def generate_desc(df):
    df.at[:,'full_desc'] = None
    new_df = df.copy(deep=True)
    for i,row in df.iterrows():
        _desc = gen_aux_desc(new_df,i)
        new_df.at[i,'full_desc'] = _desc
    return new_df

def filter_by_hs(df):
    starting_digits = [44,9401,9401,9403,9403,9403,9403,9201 ,9202,9205,9206,9207,9209,9209,9506]
    parts = []
    for s in starting_digits:
        pattern_1 = '^' + str(s)
        pattern_2 = '^' + str(s+1)
        z = df.hs_code.str.contains(pattern_1)
        for i, v in z.iteritems():
            if v == True:
                start = i
                break

        z = df.hs_code.str.contains(pattern_2)
        for i, v in z.iteritems():
            if v == True:
                end = i-1
                break
        part  = pd.DataFrame(df[start:end])
        part.reset_index(inplace=True)
        del part['level_0']
        del part['index']
        parts.append(part)

    new_df = pd.concat(parts)
    print(new_df.columns)
    return new_df

def process():
    file_loc = 'clean_files'
    file_name = '2017_02_02.csv'
    df = get_df(file_loc,file_name)
    df = generate_desc(df)
    df = df.rename(columns={'HTS Number':'hs_code'})
    df = filter_by_hs(df)
    df.to_csv(file_name)

process()