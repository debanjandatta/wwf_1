import pandas as pd
import spacy

nlp = spacy.load('en')


def get_df(file_loc='dirty_files'):
    file_name = 'CommonNames_and_status.csv'
    file_path = file_loc + '/' + file_name
    df = pd.read_csv(file_path)
    return df


def save_df(df, file_loc='clean_files'):
    file_name = 'CommonNames_and_status.csv'
    file_path = file_loc + '/' + file_name
    df.to_csv(file_path)
    return


def get_sc_name_parts(sc_nm):
    variation_kw = 'var.'
    subspecies_kw = 'ssp.'
    sub_sp = None
    genus = None
    species = None

    # check for subspecies
    if subspecies_kw in sc_nm:
        parts = sc_nm.split(subspecies_kw)
        sub_sp = str(parts[1]).lower().strip()
        sc_nm = parts[0]

    # check for variation
    if variation_kw in sc_nm:
        parts = sc_nm.split(variation_kw)
        # 1st part should have gen+species
        # 2nd part should be species
        genus = (parts[0].split(' '))[0]
        species = []
        sp = (parts[0].split(' '))[1]
        sp = sp.strip()
        species.append(sp)
        sp = parts[1]
        sp = sp.strip()
        species.append(sp)
        species = list(set(species))
        species = ';'.join(species)
    else:
        parts = sc_nm.split(' ')
        genus = parts[0]
        species = parts[1]
    return genus, species, sub_sp


def get_sc_name_parts_df(row):
    sc_nm = str(row['scientific_name'])
    genus, species, sub_sp = get_sc_name_parts(sc_nm)
    row['genus'] = genus
    row['species'] = species
    row['sub_species'] = sub_sp
    return row


def separate_sc_name():
    df = get_df()
    df = df.apply(get_sc_name_parts_df, axis=1)
    file_loc = 'clean_files'
    save_df(df,file_loc)



def collate():

    df = get_df(file_loc='clean_files')

    def replace_LR_cd(row):
        if row['Status code'] == 'LR/cd':
            return 'LC'
        else:
            return row['Status code']


    df['Status code'] = df.apply(replace_LR_cd, axis=1)

    grp_obj = df.groupby(['id'])
    all_ids = list(set(df['id']))

    columns = ['id', 'genus', 'species', 'sub_species', 'main_common_name', 'other_common_names']
    new_df = pd.DataFrame(columns=columns)

    for id in all_ids:

        tmp_df = pd.DataFrame(grp_obj.get_group(id))
        tmp_df.reset_index(inplace=True)
        del tmp_df['index']
        other_common_names = tmp_df['Common Name']
        main_common_name = tmp_df['main_common_name']
        main_common_name = list(set(main_common_name))
        other_common_names = list(set(other_common_names))


        if len(main_common_name) > 0:
            mcn = []
            for nm in main_common_name:
                if type(nm) == str:
                    mcn.append(nm)
            if len(mcn) == 0:
                main_common_name = None
            else :
                main_common_name = ';'.join(mcn)
        else:
            main_common_name = None

        ocn = []
        for k in other_common_names:
            if type(k) == str:
                ocn.append(k)
        other_common_names = ocn

        if len(other_common_names) > 0:
            other_common_names = ';'.join(other_common_names)
        else:
            other_common_names = None
        genus = tmp_df.loc[0, 'genus']
        species = tmp_df.loc[0, 'species']
        sub_species = tmp_df.loc[0, 'sub_species']
        status = tmp_df.loc[0, 'Status code']


        d = {
            'id': id,
            'genus': genus,
            'species': species,
            'sub_species': sub_species,
            'main_common_name': main_common_name,
            'other_common_names': other_common_names,
            'status': status
        }
        print (d)
        new_df = new_df.append(d, ignore_index=True)

    file_loc = 'clean_files'
    new_df.to_csv(file_loc + '/collated_names_and status.csv')
    return

separate_sc_name()
collate()