import pandas as pd
import networkx as nx
from pprint import pprint
from collections import OrderedDict
import matplotlib.pyplot as plt
import os
# HS Code node type = 1

def get_hsc_df():

    file = './../../Data_1/HS_code_descriptions/hs_codes_file_4.csv'
    df = pd.read_csv(file)
    del df['Unnamed: 0']
    return df

def get_entity_df():
    file1 = './../../Data_1/TimberBackground/commercially_traded_entity.csv'
    df1 = pd.read_csv(file1)
    del df1['Unnamed: 0']
    file2 = './../../Data_1/TimberBackground/redlist_entity.csv'
    df2 = pd.read_csv(file2)
    del df2['Unnamed: 0']
    file3 = './../../Data_1/TimberBackground/cites_entity.csv'
    df3 = pd.read_csv(file3)
    del df3['Unnamed: 0']

    return df1, df2 , df3


# --------------------- #
class hscode_node:
    def __init__(self, hs_code = None):
        self.hs_code = hs_code
        self.id = hs_code
        self.type = 1
        self.is_root = False
        self.is_leaf = False
        self.kws = []
        return

    def set_keywords(self,kws):
        if len(self.kws) == 0:
            if kws == None :
                return
            elif type(kws) ==  str:
                self.kws.append(kws)
            else :
                self.kws.extend(kws)
        else :
            self.kws.extend(kws)
# --------------------- #

# --------------------- #
class entity_node:
    def __init__(self, entity_id = None):
        self.entity_id = entity_id
        self.id = entity_id
        self.type = 2
        self.com_names = []
        self.sc_names = []
        self.kws = []

        self.is_root = False
        self.is_leaf = False
        return

    # snc : String
    def set_sc_names(self,scn):
        if type(scn) == str :
            self.sc_names.append(scn)

    def set_common_names(self, cn):
        if type(cn) == str:
            self.com_names.append(cn)
        else :
            cn = cn.split(';')
            for _item in cn:
                self.com_names.append(_item)
        return

    def set_kws(self,kws):
        if type(kws) == str :
            self.kws.append(kws)
        else :
            self.kws.extend(kws)
        return

# --------------------- #

def draw_graph(G):
    nx.drawing.nx_pylab.draw_spring(G)
    plt.show()

def find_node_by_id(G,id,type):
    all_nodes = G.nodes()
    for n in all_nodes :
        if n.id == id and n.type==type :
            return n
    return  None

# --------------------- #

def find_hsc_ancestor(graph_obj, root, idx, df):

    # --------------------- #
    def hsc_is_child(candidite_hsc, hs_code):
        result = False
        if len(hs_code) > len(candidite_hsc):
            prefix = hs_code[0:len(candidite_hsc)]
            if prefix == candidite_hsc:
                return True
        return result
    # --------------------- #
    cur_row = df.loc[idx]
    cur_indent = cur_row['Indent']
    hs_code = cur_row['hs_code']
    res = root
    if cur_indent == 0:
        return root

    # find the hs code with indent = cur_indent-1
    for i in range(idx, -1, -1):
        _indent = df.loc[i, 'Indent']
        candidite_hsc = df.loc[i, 'hs_code']

        if type(candidite_hsc) != str:
            continue

        if _indent < cur_indent and hsc_is_child(candidite_hsc, hs_code):
            all_nodes = graph_obj.nodes()
            for n in all_nodes:
                if n.hs_code == candidite_hsc:
                    res = n
                    return res
    return res
# --------------------- #

def create_hsc_subgraph(G):
    df = get_hsc_df()
    root_node = hscode_node(0)
    root_node.is_root = True
    G.add_nodes_from([root_node])

    for idx, row in df.iterrows():
        hs_code = row['hs_code']
        if type(hs_code) != str:
            continue
        # create_node
        node_obj = hscode_node(hs_code)
        node_obj.is_leaf = True
        keywords = row['pos_kw']
        keywords = keywords.split(';')
        node_obj.set_keywords(keywords)
        G.add_nodes_from([node_obj])
        anc = find_hsc_ancestor(G, root_node, idx, df)
        G.add_edge(node_obj, anc)
        anc.is_leaf = False
    return G

# --------------------- #

def create_entity_subgraph(G):
    comm_df , redlist_df , cites_df  = get_entity_df()

    # create nodes , using comm_df
    for idx ,row in comm_df.iterrows():
        entity_id = row['entity_id']
        node = entity_node(entity_id)
        G.add_nodes_from([node])
        num_scn = int(row['num_sc_names'])
        for j in range(num_scn):
            tmp = row['sc_name_'+str(j+1)]
            node.set_sc_names(tmp)
        # coniferous keyword
        is_con = row['is_coniferous']
        if is_con == 0 :
            kw = 'nonconiferous'
        else:
            kw = 'coniferous'
        node.set_kws(kw)

    # redlist_df
    for idx ,row in redlist_df.iterrows():
        entity_id = row['entity_id']
        node = find_node_by_id(G,entity_id,type=2)
        main_cn = row['main_common_name']
        other_cn = row['other_common_names']
        if main_cn is not None and type(main_cn) == str:
            node.set_common_names(main_cn)
        if other_cn is not None:
            if type(other_cn)== str:
                node.set_common_names(other_cn)
        else:
            other_cn = other_cn.split(';')
            node.set_common_names(other_cn)

    return G
# --------------------- #
def add_hsc_entity_edges(G):
    """

    :type G: object
    """
    hsc_nodes = []
    entity_nodes = []

    for n in G.nodes():
        if n.type == 1 and n.is_leaf==True :
            hsc_nodes.append(n)
        if n.type == 2:
            entity_nodes.append(n)

    for en in entity_nodes :
        # search for the keywords in en node among the ones in hsc nodes
        # exclude some words like 'spp'
        exclude_kws = ['spp']
        # Todo expand that search list later
        # 1. scientific names
        # 2. common names
        # 3. keywords

        cand_scn = []   # weight 1.0
        cand_gn = []    # weight 0.8
        cand_sp = []    # weight 1.0
        cand_cn = []    # weight 1.0
        cand_kw = []    # weight 0.01

        tmp = en.sc_names
        for t in tmp:
            t = t.lower()
            cand_scn.append(t)
            # check num of parts
            parts = t.split(' ')
            if t.count(' ') == 0:
                cand_gn.append(parts[0])
            elif t.count(' ') == 1 :
                cand_gn.append(parts[0])
                cand_sp.append(parts[1])
            elif t.count(' ') == 2 :
                cand_gn.append(parts[0])
                cand_sp.append(parts[1])
                cand_sp.append(parts[2])
                cand_sp.append(' '.join(parts[1:2]))

        tmp = en.com_names
        for t in tmp :
            t = t.lower()
            cand_cn.append(t)
        cand_kw = en.kws

        for hsc_node in hsc_nodes :
            
            hs_kws = hsc_node.kws

            wt = 0.0
            for h in hs_kws:

                h = h.replace(' spp','')

                if h in cand_scn:
                    wt += 1.0
                elif h in cand_gn:
                    wt += 0.8
                elif h in cand_sp:
                    wt += 1.0
                elif h in cand_cn:
                    wt += 1.0
                elif h in cand_kw:
                    wt += 0.1
            if wt > 0.5 :
                print ('Adding edge')
                print (hsc_node.id , en.id , wt)
                G.add_edge(hsc_node,en, attr = {'weight': wt})

    return G

# --------------------- #
def create_hs_code_nodes():
    G = nx.Graph()

    G = create_hsc_subgraph(G)
    G = create_entity_subgraph(G)
    G = add_hsc_entity_edges(G)
    draw_graph(G)
    # plot_deg_dist(G)
    return G

def plot_deg_dist(G):
    entity_nodes = []

    for n in G.nodes():
        if n.type == 2:
            entity_nodes.append(n)

    res = []
    vals =  G.degree(entity_nodes)
    for v in vals:
        res.append(v[1])

    vals = list(sorted(res,reverse=True))
    plt.hist(vals,bins=50,color='orange')
    plt.title(' Histogram of degrees of the enytity nodes ')
    plt.show()


create_hs_code_nodes()