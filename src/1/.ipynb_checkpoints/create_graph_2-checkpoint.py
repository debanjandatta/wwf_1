import pandas as pd
import networkx as nx
from pprint import pprint
from collections import OrderedDict
import matplotlib.pyplot as plt
import os

def draw_graph(G):
    nx.drawing.nx_pylab.draw_spring(G,node_size=25)
    plt.show()

def draw_sub_graph(G,component_nodes):
    sG = G.subgraph(component_nodes)
    pos = nx.fruchterman_reingold_layout(sG)
    all_nodes = sG.nodes()
    labels = {}
    for n in all_nodes:
        labels[n] = n.id

    #color by type
    type1 = []
    type2 = []

    for n in all_nodes:
        if n.type == 1 :
            type1.append(n)
        else :
            type2.append(n)

    nx.draw_networkx_nodes(
        sG,
        pos,
        nodelist=type1,
        node_color='r',
        node_size=300,
        alpha=0.8
    )
    nx.draw_networkx_nodes(
        sG,
        pos,
        nodelist=type2,
        node_color='b',
        node_size=250,
        alpha=0.25
    )
    nx.draw_networkx_edges(sG, pos, width=0.75, alpha = 0.5)
    nx.draw_networkx_labels(sG, pos, labels, font_size=16)
    plt.show()

def describe_component(G,component_nodes):

    sG = G.subgraph(component_nodes)
    all_nodes = sG.nodes()
    labels = {}
    for n in all_nodes:
        labels[n] = n.id

    # color by type
    type1 = []
    type2 = []

    for n in all_nodes:
        if n.type == 1:
            type1.append(n)
        else:
            type2.append(n)

    print('Component / Subgraph ')
    print ('Number of HS Codes ', len(type1))
    print ('Number of Entities ', len(type2))
    print ('Number of connections' , sG.number_of_edges() )
    for n in type1:
        print (' [HS code]: ' , n.id)
    for n in type2:
        print (' [entity id]' , n.id , n.sc_names)
    print ('----------')

# --------------------- #

def get_hsc_df():
    file = './../../Data_1/HS_code_descriptions/hs_codes_file_4.csv'
    df = pd.read_csv(file, index_col=0)
    return df


def get_entity_df():
    # Columns :
    # entity_id, listing
    file1 = './../../Data_1/TimberBackground/commercially_traded_entity.csv'
    df1 = pd.read_csv(file1, index_col=0)

    # Columns :
    # entity_id,is_coniferous,sc_name_1,sc_name_2,sc_name_3,type,family,num_sc_names
    file2 = './../../Data_1/TimberBackground/redlist_entity.csv'
    df2 = pd.read_csv(file2, index_col=0)

    # Columns :
    # entity_id, red_list_id, main_common_name, other_common_nmaes, status, other_common_names
    file3 = './../../Data_1/TimberBackground/cites_entity.csv'
    df3 = pd.read_csv(file3, index_col=0)

    return df1, df2, df3


# --------------------- #

class hscode_node:
    def __init__(self, hs_code=None):
        self.hs_code = hs_code
        self.id = hs_code
        self.type = 1
        self.is_root = False
        self.is_leaf = False
        self.is_confireous = False
        self.kws = []
        self.genus = []
        self.sc_name = []
        self.common_name = []
        return

    def set_coniferous(self):
        if 'coniferous' in self.kws.split(';'):
            self.is_confireous = True
        return

    def set_keywords(self, kws):
        if len(self.kws) == 0:
            if kws == None:
                return
            elif type(kws) == str:
                self.kws.append(kws)
            else:
                self.kws.extend(kws)
        else:
            self.kws.extend(kws)

    def set_genus(self,g):
        if g is None or type(g)!=str:
            return
        g = g.split(';')
        # check for duplicates
        g = list(set(g))
        self.genus.extend(g)
        return

    def set_sc_name(self, s):
        if s is None or type(s) != str:
            return
        s = s.split(';')
        # check for duplicates
        s = list(set(s))
        self.sc_name.extend(s)
        return

    def set_common_names(self, c):
        if c is None or type(c) != str:
            return
        c = c.split(';')
        # check for duplicates
        c = list(set(c))
        # set to lower case
        c = [ i.lower() for i in c]
        self.common_name.extend(c)
        return

# --------------------- #


# --------------------- #
class entity_node:

    def __init__(self, entity_id=None):
        self.entity_id = entity_id
        self.id = entity_id
        self.type = 2
        self.com_names = []
        self.sc_names = []
        self.kws = []
        self.is_coniferous = False
        self.is_root = False
        self.is_leaf = False
        return

    # snc : String
    def set_sc_names(self, scn):
        if type(scn) == str:
            self.sc_names.append(scn)

    def set_common_names(self, cn):
        if type(cn) == str:
            self.com_names.append(cn)
        else:
            cn = cn.split(';')
            for _item in cn:
                _item = _item.lower()
                self.com_names.append(_item)
        return

    def set_kws(self, kws):
        if type(kws) == str:
            self.kws.append(kws)
        else:
            self.kws.extend(kws)
        return

    def set_coniferous(self, r):
        self.is_coniferous = r


# --------------------- #

def find_node_by_id(G, id, type):
    all_nodes = G.nodes()
    for n in all_nodes:
        if n.id == id and n.type == type:
            return n
    return None


# --------------------- #

def find_hsc_ancestor(graph_obj, root, idx, df):
    # --------------------- #
    def hsc_is_child(candidite_hsc, hs_code):
        result = False
        if len(hs_code) > len(candidite_hsc):
            prefix = hs_code[0:len(candidite_hsc)]
            if prefix == candidite_hsc:
                return True
        return result

    # --------------------- #
    cur_row = df.loc[idx]
    cur_indent = cur_row['Indent']
    hs_code = cur_row['hs_code']
    res = root
    if cur_indent == 0:
        return root

    # find the hs code with indent = cur_indent-1
    for i in range(idx, -1, -1):
        _indent = df.loc[i, 'Indent']
        candidite_hsc = df.loc[i, 'hs_code']

        if type(candidite_hsc) != str:
            continue

        if _indent < cur_indent and hsc_is_child(candidite_hsc, hs_code):
            all_nodes = graph_obj.nodes()
            for n in all_nodes:
                if n.hs_code == candidite_hsc:
                    res = n
                    return res
    return res


# --------------------- #

def create_hsc_subgraph(G):
    df = get_hsc_df()
    root_node = hscode_node(0)
    root_node.is_root = True
    G.add_nodes_from([root_node])

    for idx, row in df.iterrows():
        hs_code = row['hs_code']
        if type(hs_code) != str:
            continue
        # create_node
        node_obj = hscode_node(hs_code)
        node_obj.is_leaf = True
        keywords = row['pos_kw']
        keywords = keywords.split(';')
        node_obj.set_keywords(keywords)

        node_obj.set_sc_name(row['sc_name'])
        node_obj.set_common_names(row['common_name'])
        node_obj.set_genus(row['genus'])

        G.add_nodes_from([node_obj])
        anc = find_hsc_ancestor(G, root_node, idx, df)
        G.add_edge(node_obj, anc)
        anc.is_leaf = False
    return G


# --------------------- #

def create_entity_subgraph(G):
    comm_df, redlist_df, cites_df = get_entity_df()

    # create nodes , using comm_df
    for idx, row in comm_df.iterrows():
        entity_id = row['entity_id']
        node = entity_node(entity_id)
        G.add_nodes_from([node])
        num_scn = int(row['num_sc_names'])

        for j in range(num_scn):
            tmp = row['sc_name_' + str(j + 1)]
            node.set_sc_names(tmp)

        # coniferous keyword
        is_con = row['is_coniferous']
        if is_con == 0:
            node.set_coniferous(False)
        else:
            node.set_coniferous(True)

    # redlist_df
    for idx, row in redlist_df.iterrows():
        entity_id = row['entity_id']
        node = find_node_by_id(G, entity_id, type=2)
        main_cn = row['main_common_name']
        other_cn = row['other_common_names']
        if main_cn is not None and type(main_cn) == str:
            node.set_common_names(main_cn)
        if other_cn is not None:
            if type(other_cn) == str:
                node.set_common_names(other_cn)
        else:
            other_cn = other_cn.split(';')
            node.set_common_names(other_cn)

    return G


# --------------------- #
def add_hsc_entity_edges(G):

    hsc_nodes = []
    entity_nodes = []

    for n in G.nodes():
        if n.type == 1 and n.is_leaf == True:
            hsc_nodes.append(n)
        if n.type == 2:
            entity_nodes.append(n)

    def add_egde_wt(G, u,v,wt):

        if G.has_edge(u,v) == False :
            G.add_edge(u, v, attr={'weight': wt})

        else:
            data = G.get_edge_data()
            old_wt = data['weight']
            wt = wt + old_wt
            attrs = {(u, v): {'weight': wt }}
            nx.set_edge_attributes(G, attrs)

        return G

    for en in entity_nodes:
        # search for the keywords in en node among the ones in hsc nodes
        # exclude some words like 'spp'
        exclude_kws = ['spp']
        # Todo expand that search list later
        # 1. scientific names
        # 2. common names
        # 3. keywords

        cand_scn = []
        cand_gn = []
        cand_cn = en.com_names

        tmp = en.sc_names
        for t in tmp:
            # check num of parts
            parts = t.split(' ')
            if t.count(' ') == 0:
                cand_gn.append(t)
            elif t.count(' ') == 1:
                cand_gn.append(parts[0])
                cand_scn.append(t)
            elif t.count(' ') == 2:
                cand_gn.append(parts[0])
                cand_scn.append(' '.join(parts[0:2]))
                cand_scn.append(t)

        # print (en.sc_names, cand_cn ,cand_gn)
        # Goto each hsc node
        # In hsc node there are 3 things in the order :
        # species  hsc_node.sc_name
        # genus hsc_node.genus
        # common name hsc_node.common_names

        sn_found = False
        cn_found = False
        gn_found = False
        flags = [ sn_found , cn_found , gn_found]
        search_space1 = [cand_scn,cand_cn ,cand_gn]


        for i in range(3):
            found = False
            for c in search_space1[i]:
                count = 0
                n_list = []
                for hsc_node in hsc_nodes:
                    search_space2 = [hsc_node.sc_name, hsc_node.common_name, hsc_node.genus]
                    cur_search =  search_space2[i]
                    if c in cur_search :
                        count+=1
                        found = True
                        # print('>>> Found  =' ,i, c , hsc_node.id, hsc_node.sc_name)
                        n_list.append(hsc_node)
                if count > 0 :
                    wt = 1/count
                    for _n in n_list:
                        print('Edge :',_n.id, en.id)
                        G = add_egde_wt(G, _n,en,wt)
            flags[i] = found

            if found == True:
                # print('Scientific names', cand_scn)
                break
    return G


# --------------------- #


def get_subgraph(G):
    all_nodes = G.nodes()
    node_list = []
    for n in all_nodes:
        # if n.type==1 and n.is_leaf==True :
        #     node_list.append(n)
        if n.type ==2 :
            deg = list(G.degree([n]))[0][1]
            if deg > 0 :
                node_list.append(n)
                nbrs = list(G[n])
                # nbrs = list(G.neighbors(n))
                for t in nbrs :
                    node_list.append(t)
    node_list = list(set(node_list))
    for n in node_list:
        print(n.type, n.id)
    sG = G.subgraph(node_list)
    print (sG.number_of_edges())
    return sG


def create_hs_code_nodes():
    G = nx.Graph()
    G = create_hsc_subgraph(G)
    G = create_entity_subgraph(G)
    G = add_hsc_entity_edges(G)
    G = get_subgraph(G)
    draw_graph(G)

    components = nx.connected_components(G)
    for c in components :
         draw_sub_graph(G, c)
        describe_component(G,c )
    return G


def plot_deg_dist(G):
    entity_nodes = []

    for n in G.nodes():
        if n.type == 2:
            entity_nodes.append(n)

    res = []
    vals = G.degree(entity_nodes)
    for v in vals:
        res.append(v[1])

    vals = list(sorted(res, reverse=True))
    plt.hist(vals, bins=100, color='orange')
    plt.title(' Histogram of degrees of the enytity nodes ')
    plt.show()


create_hs_code_nodes()
