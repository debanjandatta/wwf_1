Base data present in Data_1

--------------

Plant species are mentioned in 3 main files.  
1. Commercially traded list CommerciallyTradedTimberSpecies_21June2018.xlsx  	
2. CITES List CITES_AppI_II_III_KingdomPlantae.csv   	
3. IUCN Redlist CommonNames_and_status.csv    	


Some points to note :  
-	[1] has been collated manually, and there are duplicates from other list.	 
	The most important columns are : 'Family', 'Scientific name ','Coniferous vs. Nonconiferous'	
- 	[2] has a field Listing , which mentions which Appendix of CITES it belongs to. Refer: https://www.cites.org/eng/disc/how.php 
	It means : I - red flag, II red flag somewhat. III country specific red flag. For now consider each as red flag, later discern using country data.   
-	[3] has IUCN Red list, and an important column here is the Status. Similar to [2], consider all as red flag except 'LR/nt', 'LC', 'LR/lc' 	




#### 
CITES API key : rJceauCewzBt3jDypnBXCAtt 





